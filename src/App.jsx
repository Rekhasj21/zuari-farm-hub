import React from "react";
import Header from "./components/Header/Header";
import Banner from "./components/Banner/Banner";
import "./App.css";

function App() {
  return (
    <div className="main-container">
      <Header />
      <Banner />
    </div>
  );
}

export default App;
