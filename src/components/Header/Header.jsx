import React from "react";
import { GiHamburgerMenu } from "react-icons/gi";
import "./Header.css";

function Header() {
  return (
    <header className="header-home">
      <div className="container-fluid">
        <div className="row">
          <div className="header-wrap">
            <div className="logo-wrap">
              <div className="logo">
                <div className="img-logo1">
                  <a href="https://zuarifarmhub.com/">
                    <img
                      src="/farmhub-logo-1.png"
                      alt="logo"
                      className="lazyloaded"
                    />
                  </a>
                </div>
              </div>
            </div>
            <div className="right-schdule">
              <div className="header-right">
                <div className="time">
                  <div className="list-time-1">INDIA'S LEADING</div>
                  <div className="list-time-2"> AGRITECH COMPANY</div>
                </div>
              </div>
              <div className="menu-right">
                <GiHamburgerMenu size={30} fill="white"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
