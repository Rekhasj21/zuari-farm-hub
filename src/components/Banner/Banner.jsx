import React, { useState, useEffect } from "react";
import { CSSTransition } from "react-transition-group";
import Slider from "react-slick";
import "./Banner.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Banner = () => {
  const [index, setCurrentIndex] = useState(0);
  let leftIndex = index;
  let rightIndex = index;

  const [leftHovered, setLeftHovered] = useState(false);
  const [rightOvered, setRightOvered] = useState(false);

  useEffect(() => {
    const handleFullscreenError = () => {
      const elem = document.documentElement;
      if (elem.requestFullscreen) {
        elem.requestFullscreen();
      } else if (elem.mozRequestFullScreen) {
        elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen();
      } else if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
      }
    };

    const slideshowComponent = document.getElementsByClassName("banner")[0];
    slideshowComponent.addEventListener(
      "autoplay-error",
      handleFullscreenError
    );

    return () => {
      slideshowComponent.removeEventListener(
        "autoplay-error",
        handleFullscreenError
      );
    };
  }, []);

  const handleRightMouseEnter = () => {
    setRightOvered(true);
  };
  const handleLeftMouseEnter = () => {
    setLeftHovered(true);
  };

  const handleRightMouseLeave = () => {
    setRightOvered(false);
  };

  const handleLeftMouseLeave = () => {
    setLeftHovered(false);
  };

  if (index === 0) {
    leftIndex = 4;
    rightIndex = 1;
  } else if (index === 1) {
    leftIndex = 0;
    rightIndex = 2;
  } else if (index === 2) {
    leftIndex = 1;
    rightIndex = 3;
  } else if (index === 3) {
    leftIndex = 2;
    rightIndex = 4;
  } else if (index === 4) {
    leftIndex = 3;
    rightIndex = 0;
  }

  const SampleNextArrow = ({ onClick }) => {
    const imageUrl = `/banner-${rightIndex}.jpg`;

    return (
      <div
        className="slick-next"
        onClick={onClick}
        onMouseEnter={handleLeftMouseEnter}
        onMouseLeave={handleLeftMouseLeave}
      >
        {leftHovered && <img src={imageUrl} alt="Next" className="bgImage" />}
      </div>
    );
  };

  const SamplePrevArrow = ({ onClick }) => {
    const imageUrl = `/banner-${leftIndex}.jpg`;

    return (
      <div
        className="slick-prev"
        onClick={onClick}
        onMouseEnter={handleRightMouseEnter}
        onMouseLeave={handleRightMouseLeave}
      >
        {rightOvered && (
          <img src={imageUrl} alt="Previous" className="bgImage" />
        )}
      </div>
    );
  };

  const settings = {
    dots: false,
    fade: true,
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 5000,
    cssEase: "linear",
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    afterChange: (index) => setCurrentIndex(index),
  };

  const animationChange = (index) => {
    setCurrentIndex(index);
  };

  return (
    <div className="banner">
      <div>
        <Slider {...settings} afterChange={(index) => animationChange(index)}>
          <div>
            <CSSTransition
              unmountOnExit
              in={index === 0 ? true : false}
              timeout={1000}
            >
              <img src="/banner-01-0.jpg" className="image banner-container" />
            </CSSTransition>

            <div className="banner-heading">
              {/* <div className="banner-text"> */}
              <h1 className={index === 0 ? "zoom-in banner-text" : ""}>
                Maximising yields through expert <br />
                Agronomy services
              </h1>
              {/* </div> */}
            </div>
          </div>
          <div>
            <CSSTransition
              unmountOnExit
              in={index === 1 ? true : false}
              timeout={1000}
            >
              <img src="/banner-01-1.jpg" className="image banner-container" />
            </CSSTransition>
            <div className="banner-heading">
              <h1 className={index === 1 ? "zoom-in-img2 banner-text" : ""}>
                Empowering farmers with <br />
                end-to-end services & solution
              </h1>
            </div>
          </div>
          <div>
            <CSSTransition
              unmountOnExit
              in={index === 2 ? true : false}
              timeout={1000}
            >
              <img src="/banner-01-2.jpg" className="image banner-container" />
            </CSSTransition>
            <div className="banner-heading">
              <h1 className={index === 2 ? "zoom-in-img3 banner-text" : ""}>
                Enchancing farm productivity <br /> through integrated Agri-tech
                solutions
              </h1>
            </div>
          </div>
          <div>
            <CSSTransition
              unmountOnExit
              in={index === 3 ? true : false}
              timeout={1000}
            >
              <img src="/banner-farm.jpg" className="image banner-container" />
            </CSSTransition>
            <div className="banner-heading">
              <h1 className={index === 3 ? "zoom-in-img4 banner-text" : ""}>
                Providing total farm inputs through <br />
                India's only agri supermart <br />
                Jai Kisan Junction
              </h1>
            </div>
          </div>
          <div>
            <CSSTransition
              unmountOnExit
              in={index === 4 ? true : false}
              timeout={1000}
            >
              <img src="/banner-01-4.png" className="image banner-container" />
            </CSSTransition>
          </div>
        </Slider>
      </div>
    </div>
  );
};

export default Banner;
